#include <suruiha_gazebo_plugins/controllers/riders_controller.h>

using namespace gazebo;
// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(RidersUAVController)

RidersUAVController::RidersUAVController(/* args */)
{
}

RidersUAVController::~RidersUAVController()
{
}

void RidersUAVController::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf) {
    this->model_ = _model;
    this->world_ = this->model_->GetWorld();

    this-> poseUpdateRate = _sdf->Get<int>("poseUpdateRate");

    // load joints
    sdf::ElementPtr jointControlSDF = _sdf->GetElement("joint_control");

    while (jointControlSDF) {
        JointControl* jointControl = new JointControl();
        jointControl->jointName = jointControlSDF->Get<std::string>("name");
        jointControl->SetJointType(jointControlSDF->Get<std::string>("type"));
        jointControl->joint = this->model_->GetJoint(jointControl->jointName);
        if (jointControl->joint == nullptr) {
            std::cout << "cannot get joint with name:" << jointControl->jointName << "\n";
        }
        this->SetPIDParams(jointControl, jointControlSDF);
        joints_.push_back(jointControl);
//            gzdbg << "joint added " << jointControl->jointName << " with type:" << jointControl->jointType << "\n";
        jointControlSDF = jointControlSDF->GetNextElement("joint_control");
    }

    this->modelName = _sdf->GetParent()->GetAttribute("name")->GetAsString();
    std::string control_topic_name = this->modelName + "_control";
    std::string pose_topic_name = this->modelName + "_pose";

    std::cout << "control_topic:" << control_topic_name << " pose_topic:" << pose_topic_name
            << std::endl;

    if (!ros::isInitialized())
    {
    int argc = 0;
    char **argv = NULL;
    ros::init(argc, argv, "",
        ros::init_options::NoSigintHandler);
    }

    this->rosNode.reset(new ros::NodeHandle(""));

    ros::SubscribeOptions so =
    ros::SubscribeOptions::create<geometry_msgs::Twist>(
        control_topic_name,
        1,
        boost::bind(&RidersUAVController::SetControl, this, _1),
        ros::VoidPtr(), NULL);

    this->control_twist_sub_ = this->rosNode->subscribe(so);

    this->pose_pub_ = this->rosNode->advertise<geometry_msgs::Pose>(pose_topic_name, 1);

    this->update_connection_ = event::Events::ConnectWorldUpdateBegin(
            boost::bind(&RidersUAVController::UpdateStates, this));

}

void RidersUAVController::SetControl(const geometry_msgs::Twist::ConstPtr& _twist) {
    targetThrottle = _twist->linear.x;
    targetPitch = _twist->angular.y;
    targetRoll = _twist->angular.x;
}

void RidersUAVController::UpdateStates() {
    common::Time currTime = this->world_->SimTime();

    double dt_pose = (currTime - lastPosePublishTime).Double() * 1000; // miliseconds
    if (dt_pose > this->poseUpdateRate) {
        ignition::math::Pose3d pose = this->model_->WorldPose();
        geometry_msgs::Pose poseMsg;
        poseMsg.position.x = pose.Pos().X();
        poseMsg.position.y = pose.Pos().Y();
        poseMsg.position.z = pose.Pos().Z();
        ignition::math::Vector3d ori = pose.Rot().Euler();
        ignition::math::Angle zOri = ori.Z() - ignition::math::Angle::Pi.Radian();
        zOri.Normalize();
        ori.Z(zOri.Radian());
        ignition::math::Quaterniond quat;
        quat.Euler(ori);
        poseMsg.orientation.x = quat.X();
        poseMsg.orientation.y = quat.Y();
        poseMsg.orientation.z = quat.Z();
        poseMsg.orientation.w = quat.W();
        this->pose_pub_.publish(poseMsg);
        lastPosePublishTime = currTime;
    }

    double dt_cmd = (currTime - lastUpdateTime).Double();
    if (lastUpdateTime.Double() == 0.0) {
        dt_cmd = 0.0;
    }

    this->CalculateJoints(targetThrottle, targetPitch, targetRoll, dt_cmd);
    this->lastUpdateTime = currTime;

}

void RidersUAVController::CalculateJoints(double targetThrottle, double targetPitch, double targetRoll, common::Time dt) {
    ignition::math::Pose3d pose = this->model_->WorldPose();
    double pitch = pose.Rot().Euler().X();
    double roll = pose.Rot().Euler().Y();

    pitch = pitch - targetPitch;
    roll = roll - targetRoll;

    joints_[0]->SetCommand(targetThrottle, dt);
    joints_[1]->SetCommand(pitch - roll, dt);
    joints_[2]->SetCommand(pitch + roll, dt);
}


void RidersUAVController::SetPIDParams(JointControl* jointControl, sdf::ElementPtr _sdf) {
    if (_sdf->HasElement("p")) {
        double p = _sdf->Get<double>("p");
        double i = _sdf->Get<double>("i");
        double d = _sdf->Get<double>("d");
        double imax = _sdf->Get<double>("imax");
        double imin = _sdf->Get<double>("imin");
        double cmdmax = _sdf->Get<double>("cmdmax");
        double cmdmin = _sdf->Get<double>("cmdmin");
        jointControl->SetPIDParams(p, i, d, imax, imin, cmdmax, cmdmin);
    }
}
