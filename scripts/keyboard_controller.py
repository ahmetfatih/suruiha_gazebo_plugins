#!/usr/bin/env python
import rospy

from geometry_msgs.msg import Twist
from std_msgs.msg import String

import math

global throttle
global pitch
global roll
global control_cmd

def key_down(msg):

    print(msg.data)
    if msg.data == "w":
        control_cmd.linear.x +=  10
    if msg.data == "s":
        control_cmd.linear.x -= 10
    if msg.data == "d":
        control_cmd.angular.x += 0.01
    if msg.data == "a":
        control_cmd.angular.x -= 0.01
    if msg.data == "e":
        control_cmd.angular.y += 0.01
    if msg.data == "q":
        control_cmd.angular.y -= 0.01


def key_up(msg):
    return
    
if __name__ == "__main__":
    rospy.init_node('keyboard_controller')
    uav_name = "zephyr_with_skid_pad"

    control_topic_name = uav_name + '_control'

    key_down_sub = rospy.Subscriber("/keyboard_down", String, key_down)
    key_up_sub = rospy.Subscriber("/keyboard_up", String, key_up)

    control_pub = rospy.Publisher(control_topic_name, Twist, queue_size=1)

    throttle = 0.0
    pitch = 0.0
    roll = 0.0

    control_cmd = Twist()

    # how many times in a second the control loop is going to run
    ros_rate = rospy.Rate(10)

    while not rospy.is_shutdown():

        control_pub.publish(control_cmd)

        ros_rate.sleep()
