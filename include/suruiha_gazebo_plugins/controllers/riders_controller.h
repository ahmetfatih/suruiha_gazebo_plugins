#ifndef RIDERS_CONTROLLER_H
#define RIDERS_CONTROLLER_H

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <ros/callback_queue.h>

#include <gazebo/common/Plugin.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/common/Time.hh>
#include <gazebo/common/Events.hh>
#include <suruiha_gazebo_plugins/controllers/joint_control.h>
#include <gazebo/sensors/Sensor.hh>
#include <gazebo/physics/physics.hh>
#include "gazebo/physics/MultiRayShape.hh"
#include "gazebo/physics/World.hh"
#include "gazebo/physics/Model.hh"
#include "gazebo/physics/Joint.hh"

namespace gazebo
{
    class RidersUAVController : public ModelPlugin
    {
        public: RidersUAVController(/* args */);
        public: ~RidersUAVController();
        public: void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);
        public: void SetControl(const geometry_msgs::Twist::ConstPtr& controlTwist);
        public: void SetPIDParams(JointControl* jointControl, sdf::ElementPtr _sdf);
        public: void UpdateStates();
		private: void CalculateJoints(double targetThrottle, double targetPitch, double targetRoll, common::Time dt);

        private: event::ConnectionPtr update_connection_;

		private: physics::WorldPtr world_;
		private: physics::ModelPtr model_;
        private: std::vector<JointControl*> joints_;

    	private: std::string modelName;

		private: std::unique_ptr<ros::NodeHandle> rosNode;
        private: ros::Subscriber control_twist_sub_;
        private: ros::Publisher pose_pub_;

        private: double targetThrottle;
        private: double targetPitch;
        private: double targetRoll;

        private: common::Time lastUpdateTime;
        private: common::Time lastPosePublishTime;
        private: int poseUpdateRate;

    };
    
    
}

#endif