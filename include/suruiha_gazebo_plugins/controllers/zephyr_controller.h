#ifndef ZEPHYR_CONTROLLER_H
#define ZEPHYR_CONTROLLER_H

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <ros/callback_queue.h>

#include <gazebo/common/Plugin.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/common/Time.hh>
#include <gazebo/common/Events.hh>
#include <suruiha_gazebo_plugins/controllers/joint_control.h>
#include <suruiha_gazebo_plugins/uav_sensor/uav_sensor.h>
#include <suruiha_gazebo_plugins/battery/battery_manager.h>
#include <gazebo/sensors/Sensor.hh>

namespace gazebo
{
	class ZephyrController : public ModelPlugin
    {

		public: ZephyrController();
		public: virtual ~ZephyrController();

		public: void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);
		protected: virtual void UpdateStates();
		public: event::ConnectionPtr update_connection_;

		public: physics::WorldPtr world_;
		public: physics::ModelPtr model_;
//		public: physics::LinkPtr bodyLink_;
//		public: std::vector<physics::JointPtr> jointPtrs;
		
//		public: std::string robot_namespace_;
//		public: std::string control_topic_name_;
//		public: std::string pose_topic_name_;
//		public: std::string user_command_topic_name_;
    	public: std::string modelName;
		public: ros::NodeHandle* rosnode_;
        public: ros::Subscriber control_twist_sub_;
//        public: ros::Subscriber user_command_sub_;
        public: ros::Publisher pose_pub_;
        public: ros::Publisher sensor_pub_;

        public: double targetThrottle;
        public: double targetPitch;
        public: double targetRoll;

        public: std::vector<JointControl*> joints_;
        public: void SetPIDParams(JointControl* jointControl, sdf::ElementPtr _sdf);
	    public: void SetControl(const geometry_msgs::Twist::ConstPtr& controlTwist);
//	    public: void ProcessUserCommand(const std_msgs::String::ConstPtr& user_command);
	    public: void CalculateJoints(double targetThrottle, double targetPitch, double targetRoll, common::Time dt);
        protected: void SetZeroForceToJoints();

		public: boost::mutex update_mutex_;
//		public: ros::CallbackQueue queue_;
//		public: boost::thread callback_queue_thread_;
//	    public: void QueueThread();
        public: UAVSensor uavSensor;
        public: common::Time lastSensorTime;
        public: int sensorUpdateReate;

        public: common::Time lastUpdateTime;

        public: common::Time lastPosePublishTime;
        public: int poseUpdateRate;

		// communcation between air traffic controller and fligh controller
		public: transport::NodePtr node;
		public: transport::SubscriberPtr airControlSubPtr;
		protected: void OnAirControlMsg(ConstAnyPtr& airControlMsg);
		public: transport::SubscriberPtr batteryReplaceSubPtr;
		protected: void OnBatteryReplaceMsg(ConstAnyPtr& batteryReplaceMsg);
        protected: bool isActive;

        protected: sensors::SensorPtr logicalCameraSensorPtr;
        protected: sensors::SensorPtr cameraSensorPtr;


		// battery manager
		protected: BatteryManager battery;

	};
}

#endif
